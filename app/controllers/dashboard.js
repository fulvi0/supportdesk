const express=require('express');
const User=require('../models/user.js');
const Group=require('../models/group.js');
// const path=require('path');

var router=express.Router();

router.route("/dashboard").get((req,res)=>{
    res.render("../app/views/admin_layout");
});

/*router.route("/").get((req,res)=>{
    res.render("../app/views/userArea/login");
});*/

module.exports = router