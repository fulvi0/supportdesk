const express=require('express');
const Group=require('../models/group.js');
// const path=require('path');

var router=express.Router();

router.route('/groupArea').get(async (req,res)=>{
    try {
        let groups=await Group.find({}).sort({name: 1 });
        res.render('../app/views/groupArea/group_admin',{docs: groups}); //docs es una variable declarada en el pug
    } catch (error) {
        console.log(error);
        res.send("error");
    }
}).post(async (req,res)=>{
    try{
        let status=await Group.addGroup(req.body.group_name, req.body.group_description);
        if (status==Group.OK){
            res.redirect("/supportdesk/groupArea");
        }
        else {
            res.send("Error creating group - Code " + status);
        }
    }
    catch(err)
    {
        console.log(err);
        res.send("Error");
    }
});

/*router.route('/groupArea/New').get(async (req,res)=>{
    try {
        res.render('../app/views/groupArea/group_add')
    } catch (error) {
        console.log(error);
        res.send("error");
    }
});*/

router.route('/groupArea/Edit/:id').get(async (req,res)=>{
    try {
        let editedgroup = await Group.findOne({_id:req.params.id});
        res.render('../app/views/groupArea/group_edit',{editedgroup:editedgroup});
    } catch (error) {
        console.log(error);
        res.send("error");
    }
}).post(async (req,res)=>{
    try{
        let status=await Group.updateGroup(req.params.id, req.body.group_name, req.body.group_description);
        if (status==Group.OK){
            res.redirect("/supportdesk/groupArea");
        }
        else {
            res.send("Error editing group - Code " + status);
        }
    }
    catch(err)
    {
        console.log(err);
        res.send("Error");
    }
});

module.exports = router