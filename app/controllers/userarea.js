const express=require('express');
const User=require('../models/user.js');
const Group=require('../models/group.js');
// const path=require('path');

var router=express.Router();

router.route("/").get((req,res)=>{
    res.redirect("supportdesk/userArea/login");
});

router.route('/userArea/login').get((req,res)=>{
    res.render('../app/views/userArea/login');
}).post(async (req,res)=>{
    let status=await User.login(req.body.username, req.body.password);
    if (status.cod==User.OK){
        res.redirect("/supportdesk/taskArea");
    } else {
        res.render('../app/views/userArea/login',{error_message:'Hmmm nope. Dont remember you. Try again'});
    }
})

router.route('/userArea/create').get(async(req,res)=>{
    let groups=await Group.find({});
    res.render('../app/views/userArea/create',{groups: groups});
}).post(async (req,res)=>{
    let status=await User.addUser(req.body.username, req.body.group, req.body.job, req.body.password, req.body.password2);
    if (status.cod==User.OK){
        res.redirect("/supportdesk/taskArea");
    } else if(status.cod==User.ERR_PWD_CORTO){
        res.send("Password is too short");
    }else {
        res.send("Error creating account - Code " + status.cod);
    }
})

router.route('/userArea').get(async (req,res)=>{
    try {
        let users=await User.find({}).populate('group').sort({name: 1 });
        res.render('../app/views/userArea/user_admin',{users: users}); //docs es una variable declarada en el pug
    } catch (error) {
        console.log(error);
        res.send("error");
    }
});

router.route('/userArea/Edit/:id').get(async (req,res)=>{
    let groups=await Group.find({}).sort({name: 1 });
    let editeduser=await User.findOne({_id:req.params.id}).populate('group');
    res.render('../app/views/userArea/user_edit',{groups:groups, editeduser: editeduser});
}).post(async (req,res)=>{
    try
    {
        let status=await User.updateUser(req.params.id, req.body.user_name, req.body.user_job, req.body.user_group);
        if (status.cod==User.OK){
            res.redirect("/supportdesk/userArea");
        }
        else {
            res.send("Error editing user - Code " + status);
        }
    }
    catch(err)
    {
        console.log(err);
        res.send("Error");
    }
});

module.exports = router