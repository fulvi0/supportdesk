const express=require('express');
const State=require('../models/state.js');
const Impact=require('../models/impact.js');
const Urgency=require('../models/urgency.js');

var router=express.Router();

router.route('/loadArea').get(async (req,res)=>{
    State.FillStates();
    Impact.FillImpacts();
    Urgency.FillUrgencies();
    res.send("State, Impact and Urgency loaded");
})

module.exports = router