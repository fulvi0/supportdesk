const express=require('express');
const Task=require('../models/task.js');
const Tasktrack=require('../models/tasktrack.js');
const Group=require('../models/group.js');
const State=require('../models/state.js');
const User=require('../models/user.js');
const Impact=require('../models/impact.js');
const Urgency=require('../models/urgency.js');
// const path=require('path');

var router=express.Router();

//Ver los tickets
router.route('/taskArea').get(async (req,res)=>{
    try {
        let taskstate = await State.find({description:{$in:["In progress","On Hold","Request to User","On review"]}});
    let tasks=await Task.find({state:taskstate/*,responsible:session._id*/}).populate('impact').populate('urgency').populate('state');
        /*tasks.sort((a,b) =>{
            a.impact.impact_value + a.urgency.urgenc_value - b.impact_value
        })*/
        res.render('../app/views/taskArea/task_admin',{docs: tasks}); //docs es una variable declarada en el pug
    } catch (error) {
        console.log(error);
        res.send("error");
    }
});

router.route('/taskArea/New').get(async (req,res)=>{
    let states=await State.find({});
    let groups=await Group.find({});
    let users=await User.find({});
    let impacts=await Impact.find({});
    let urgencies=await Urgency.find({});
    res.render('../app/views/taskArea/task_add',{states: states, groups: groups, users: users, impacts: impacts, urgencies: urgencies});
}).post(async (req,res)=>{
    try
    {
        let status=await Task.addTask(req.body.task_name, req.body.task_eta, req.body.task_impact, req.body.task_urgency, req.body.task_description, req.body.task_resolution, req.body.task_state, req.body.task_group, /*req.body.task_requester*/req.body.task_responsible, req.body.task_responsible);
        if (status==Task.OK){
            res.redirect("/supportdesk/taskArea");
        }
        else {
            res.send("Error creating task - Code " + status);
        }
    }
    catch(err)
    {
        console.log(err);
        res.send("Error");
    }
})

router.route('/taskArea/Edit/:id').get(async (req,res)=>{
    let states=await State.find({});
    let groups=await Group.find({});
    let users=await User.find({});
    let impacts=await Impact.find({});
    let urgencies=await Urgency.find({});
    let editedtask=await Task.findOne({_id:req.params.id}).populate('requester').populate('impact').populate('urgency').populate('state').populate('group').populate('responsible');
    let editedtasktracks= await Tasktrack.find({task:req.params.id}).sort({change_date: -1 });
    res.render('../app/views/taskArea/task_edit',{states: states, groups: groups, users: users, impacts: impacts, urgencies: urgencies, editedtask: editedtask, tracks:editedtasktracks});
}).post(async (req,res)=>{
    try
    {
        let status=await Task.updateTask(req.params.id, req.body);
        if (status==Task.OK){
            //res.send("Task Created");
            res.redirect("/supportdesk/taskArea");
        }
        else {
            res.send("Error editing task - Code " + status);
        }
    }
    catch(err)
    {
        console.log(err);
        res.send("Error");
    }
});



//Ver los tickets que le deben al usuario
router.route('/taskArea/Debts').get(async (req,res)=>{
    try {
        let taskstate = await State.find({description:{$in:["In progress","On Hold","Request to User","On review"]}});
        let tasks=await Task.find({state:taskstate/*,requester:session._id*/}).populate('impact').populate('urgency').populate('state').populate('responsible');
        /*tasks.sort((a,b) =>{
            a.impact.impact_value + a.urgency.urgenc_value - b.impact_value
        })*/
        res.render('../app/views/taskArea/task_debt_admin',{docs: tasks}); //docs es una variable declarada en el pug
    } catch (error) {
        console.log(error);
        res.send("error");
    }
});

router.route('/taskArea/Debts/Edit/:id').get(async (req,res)=>{
    let states=await State.find({});
    let groups=await Group.find({});
    let users=await User.find({});
    let impacts=await Impact.find({});
    let urgencies=await Urgency.find({});
    let editedtask=await Task.findOne({_id:req.params.id}).populate('requester').populate('impact').populate('urgency').populate('state').populate('group').populate('responsible');
    let editedtasktracks= await Tasktrack.find({task:req.params.id}).sort({change_date: -1 });
    res.render('../app/views/taskArea/task_debt_edit',{states: states, groups: groups, users: users, impacts: impacts, urgencies: urgencies, editedtask: editedtask, tracks:editedtasktracks});
}).post(async (req,res)=>{
    try
    {
        let status=await Task.updateTask(req.params.id, req.body);
        if (status==Task.OK){
            //res.send("Task Created");
            res.redirect("/supportdesk/taskArea/Debts");
        }
        else {
            res.send("Error editing task - Code " + status);
        }
    }
    catch(err)
    {
        console.log(err);
        res.send("Error");
    }
});


//Ver los tickets cerrados
router.route('/taskArea/History').get(async (req,res)=>{
    try {
        let taskstate = await State.find({description:{$in:["Closed","Discarded"]}});
        let tasks=await Task.find({state:taskstate/*,responsible:session._id*/}).populate('impact').populate('urgency').populate('state');
        /*tasks.sort((a,b) =>{
            a.impact.impact_value + a.urgency.urgenc_value - b.impact_value
        })*/
        res.render('../app/views/taskArea/task_history_admin',{docs: tasks}); //docs es una variable declarada en el pug
    } catch (error) {
        console.log(error);
        res.send("error");
    }
});


router.route('/taskArea/History/View/:id').get(async (req,res)=>{
    let states=await State.find({});
    let groups=await Group.find({});
    let users=await User.find({});
    let impacts=await Impact.find({});
    let urgencies=await Urgency.find({});
    let editedtask=await Task.findOne({_id:req.params.id}).populate('requester').populate('impact').populate('urgency').populate('state').populate('group').populate('responsible');
    let editedtasktracks= await Tasktrack.find({task:req.params.id}).sort({change_date: -1 });
    res.render('../app/views/taskArea/task_history_view',{states: states, groups: groups, users: users, impacts: impacts, urgencies: urgencies, editedtask: editedtask, tracks:editedtasktracks});
}).post(async (req,res)=>{
    try
    {
        res.redirect("/supportdesk/taskArea/History");
    }
    catch(err)
    {
        console.log(err);
        res.send("Error");
    }
});

module.exports = router