var User=require("../models/user.js");

module.exports = async function(req,res,next){
    if (req.session && req.session.user_id){
        console.log("session: " + req.session.user_id );
        var u=await User.findById(req.session.user_id);
        if (u){
            res.locals.nombreUsuario=u.usuario;
        }
        next();
    } else {
        console.log ("sesion no establecida");
        res.redirect("/supportdesk/userArea/login");
    }
}