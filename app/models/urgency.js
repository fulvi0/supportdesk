const path=require('path');
const db=require(path.join(__dirname, '../../source', 'db.js'));

var urgencySchema={
    urgency_value:{
        type: Number,
        required: "El valor es requerido"
    },
    description:{
        type: String,
        maxlength:30,
        minlength:5,
        required: "La descripcion es requerida",
        unique:true
    },
    tasks: [{type:db.Schema.ObjectId, ref: "Task"}]
}

var urgency_schema=new db.Schema(urgencySchema); //Crear schema
var Urgency=db.model("Urgency", urgency_schema); //Crear o asumir modelo/coleccion en Mongo


Urgency.OK=0;
Urgency.ERR_DB=1;

//funciones
Urgency.addUrgency=async (value,desc) => {
    let newUrgency = new Urgency({urgency_value:value, description:desc}) //Crear objeto Urgency con el input
    try{
        await newUrgency.save();
        console.log("Urgencia creada en base");
        return Urgency.OK;
    }
    catch(err){
        console.log(err);
        return Urgency.ERR_DB;
    }
}

Urgency.updateUrgency=async (urgency_id, value, desc) => {
    //let newUrgency = new Urgency({_id:urgency_id, urgency_value:value, description:desc}) //Crear objeto Urgency con el input
    let newUrgency = Urgency.findOne({_id:urgency_id});

    newUrgency.urgency_value = value;
    newUrgency.description = desc;

    try{
        await newUrgency.save();
        console.log("Estado actualizado en base");
        return Urgency.OK;
    }
    catch(err){
        console.log(err);
        return Urgency.ERR_DB;
    }
}

Urgency.deleteUrgency=async (urgency_id) => {
    try{
        await Urgency.deleteOne({_id:urgency_id});
        console.log("Urgencia borrado en base");
        return Urgency.OK;
    }
    catch(err){
        console.log(err);
        return Urgency.ERR_DB;
    }
}

Urgency.FillUrgencies=async () => {

    await Urgency.deleteMany({}); //Borrar los valores actuales

    let Urgencies=[
        {urgency_value: '0', description: 'Fallo Total'},
        {urgency_value: '1', description: 'Fallo Parcial'},
        {urgency_value: '2', description: 'Requerimiento de Cambio'},
        {urgency_value: '3', description: 'Requerimiento de Informacion'},
        {urgency_value: '4', description: 'Otros requerimientos'}
    ];

    for (urgency of Urgencies) {
        let newUrgency = new Urgency(urgency);
        await newUrgency.save();
    }
}

module.exports=Urgency;