const path=require('path');
const db=require(path.join(__dirname, '../../source', 'db.js'));
var task=require("./taskinfo.js");
var state=require("./state.js");
var group=require("./group.js");
var user=require("./user.js");
var impact=require("./impact.js");
var urgency=require("./urgency.js");

var tasktrackSchema={
    change_date:{
        type: Date,
        requeried: "La fecha de creacion es requerida"
    },
    type:{
        type: String,
        maxlength: 20,
        required: "El tipo es requerido"
    },
    description:{
        type: String,
        maxlength:2000,
        minlength:5,
        required: "La descripcion es requerida"
    },
    task: {type: db.Schema.Types.ObjectId, ref:"Task"}//,
    //tasks: [{type:db.Schema.ObjectId, ref: "Task"}]
}

var tasktrack_schema=new db.Schema(tasktrackSchema); //Crear schema
var Tasktrack=db.model("TaskTrack", tasktrack_schema); //Crear o asumir modelo/coleccion en Mongo


Tasktrack.OK=0;
Tasktrack.ERR_DB=1;

//funciones
Tasktrack.addTasktrack=async (chg_date, type, desc, task_id) => {
    let newTask = await task.findOne({_id:task_id}); //Buscar el task
    let newTasktrack = new Tasktrack({change_date:chg_date, type:type, description:desc}) //Crear objeto Tasktrack con el input
    
    newTasktrack.task = newTask;
    newTask.tracks.Push(newTasktrack);
    
    try{
        await newTask.save();
        await newTasktrack.save();
        console.log("Tracking creado en base");
        return Tasktrack.OK;
    }
    catch(err){
        console.log(err);
        return Tasktrack.ERR_DB;
    }
}

Tasktrack.updateTasktrack=async (id, Modified_Task) => {
    let newTasktrack = new Tasktrack({_id:id, description:Modified_Task}) //Crear objeto Tasktrack con el input
    try{
        await Tasktrack.findByIdAndUpdate(id,newTasktrack);
        console.log("Estado actualizado en base");
        return Tasktrack.OK;
    }
    catch(err){
        console.log(err);
        return Tasktrack.ERR_DB;
    }
}

Tasktrack.deleteTasktrack=async (Tasktrack_id) => {
    try{
        await Tasktrack.deleteOne({_id:Tasktrack_id});
        console.log("Estado borrado en base");
        return Tasktrack.OK;
    }
    catch(err){
        console.log(err);
        return Tasktrack.ERR_DB;
    }
}

//Agregar un Task Track sin actualizar el Task
Tasktrack.addTasktrackNoPush=async (chg_date, type, desc, modifiedTask) => {
    let newTasktrack = new Tasktrack({change_date:chg_date, type:type, description:desc}) //Crear objeto Tasktrack con el input

    newTasktrack.task = modifiedTask;

    try{
        modifiedTask.tracks.push(newTasktrack);
        await newTasktrack.save();
        console.log("Tracking creado en base");
        return {cod:Tasktrack.OK, Track:newTasktrack};
    }
    catch(err){
        console.log(err);
        return {cod:Tasktrack.ERR_DB, Track:""};
    }
}

//Este metodo se llama cuando se va a modificar o agregar un task
//El mismo agrega todos los tracks por cada cambio en el task
Tasktrack.addMultipleTasktracks=async (modifiedTask, ModificationDate) => {
    let originalTask = await task.findOne({_id:modifiedTask._id}); //Buscar el task como esta en la base de datos
    
    if (originalTask){ //Si el Task existe
        let originaltaskGroup = await group.findOne({_id:originalTask.group}); //Buscar grupo
        let originaltaskState = await state.findOne({_id:originalTask.state}); //Buscar estado
        let originaltaskRequester = await user.findOne({_id:originalTask.requester}); //Buscar requester
        let originaltaskResponsible = await user.findOne({_id:originalTask.responsible}); //Buscar responsable
        let originaltaskImpact = await impact.findOne({_id:originalTask.impact}); // Buscar Impacto
        let originaltaskUrgency = await urgency.findOne({_id:originalTask.urgency}); // Buscar Urgencias

        if (originalTask.name != modifiedTask.name){
            let status = await Tasktrack.addTasktrackNoPush(ModificationDate,'Name','Name changed from "' + originalTask.name + '" to "' + modifiedTask.name + '".', modifiedTask); //Agregar el nuevo Track
            if (status.cod!=Tasktrack.OK) {
                return {cod:task.ERR_DB, task:""}; //Si no se pudo agregar el Track, mandar el error al Task
            }
        }
        if(originalTask.eta != modifiedTask.eta){
            let status = await Tasktrack.addTasktrackNoPush(ModificationDate,'ETA','ETA changed from "' + originalTask.eta + '" to "' + modifiedTask.eta + '".', modifiedTask); //Agregar el nuevo Track
            if (status.cod!=Tasktrack.OK) {
                return {cod:task.ERR_DB, task:""}; //Si no se pudo agregar el Track, mandar el error al Task
            }
        }
        //En el caso de las fechas se hace de esta manera porque una comparacion directa siempre da falso
        if(modifiedTask.eta_date-originalTask.eta_date != 0){
            let status = await Tasktrack.addTasktrackNoPush(ModificationDate,'Deadline','Deadline changed from "' + originalTask.eta_date + '" to "' + modifiedTask.eta_date + '".', modifiedTask); //Agregar el nuevo Track
            if (status.cod!=Tasktrack.OK) {
                return {cod:task.ERR_DB, task:""}; //Si no se pudo agregar el Track, mandar el error al Task
            }
        }
        if(originalTask.closed_date != modifiedTask.closed_date){
            let status = await Tasktrack.addTasktrackNoPush(ModificationDate,'Closed Date','Closed Date changed from "' + originalTask.closed_date + '" to "' + modifiedTask.closed_date + '".', modifiedTask); //Agregar el nuevo Track
            if (status.cod!=Tasktrack.OK) {
                return {cod:task.ERR_DB, task:""}; //Si no se pudo agregar el Track, mandar el error al Task
            }
        }
        if(originalTask.description != modifiedTask.description){
            let status = await Tasktrack.addTasktrackNoPush(ModificationDate,'Description','Description changed from "' + originalTask.description + '" to "' + modifiedTask.description + '".', modifiedTask); //Agregar el nuevo Track
            if (status.cod!=Tasktrack.OK) {
                return {cod:task.ERR_DB, task:""}; //Si no se pudo agregar el Track, mandar el error al Task
            }
        }
        if(originalTask.resolution != modifiedTask.resolution){
            let status = await Tasktrack.addTasktrackNoPush(ModificationDate,'Resolution','Resolution changed from "' + originalTask.resolution + '" to "' + modifiedTask.resolution + '".', modifiedTask); //Agregar el nuevo Track
            if (status.cod!=Tasktrack.OK) {
                return {cod:task.ERR_DB, task:""}; //Si no se pudo agregar el Track, mandar el error al Task
            }
        }
        if(originaltaskGroup.name != modifiedTask.group.name){
            let status = await Tasktrack.addTasktrackNoPush(ModificationDate,'Group','Group changed from "' + originaltaskGroup.name + '" to "' + modifiedTask.group.name + '".', modifiedTask); //Agregar el nuevo Track
            if (status.cod!=Tasktrack.OK) {
                return {cod:task.ERR_DB, task:""}; //Si no se pudo agregar el Track, mandar el error al Task
            }
        }
        if(modifiedTask.state.description != originaltaskState.description){
            let status = await Tasktrack.addTasktrackNoPush(ModificationDate,'State','State changed from "' + originaltaskState.description + '" to "' + modifiedTask.state.description + '".', modifiedTask); //Agregar el nuevo Track
            if (status.cod!=Tasktrack.OK) {
                return {cod:task.ERR_DB, task:""}; //Si no se pudo agregar el Track, mandar el error al Task
            }
        }
        if(modifiedTask.requester.name != originaltaskRequester.name){
            let status = await Tasktrack.addTasktrackNoPush(ModificationDate,'Requester','Requester changed from "' + originaltaskRequester.name + '" to "' + modifiedTask.requester.name  + '".', modifiedTask); //Agregar el nuevo Track
            if (status.cod!=Tasktrack.OK) {
                return {cod:task.ERR_DB, task:""}; //Si no se pudo agregar el Track, mandar el error al Task
            }
        }
        if(modifiedTask.responsible.name != originaltaskResponsible.name){
            let status = await Tasktrack.addTasktrackNoPush(ModificationDate,'Responsible','Responsible changed from "' + originaltaskResponsible.name + '" to "' + modifiedTask.responsible.name + '".', modifiedTask); //Agregar el nuevo Track
            if (status.cod!=Tasktrack.OK) {
                return {cod:task.ERR_DB, task:""}; //Si no se pudo agregar el Track, mandar el error al Task
            }
        }
        if(modifiedTask.impact.description != originaltaskImpact.description){
            let status = await Tasktrack.addTasktrackNoPush(ModificationDate,'Impact','Impact changed from "' + originaltaskImpact.description + '" to "' + modifiedTask.impact.description + '".', modifiedTask); //Agregar el nuevo Track
            if (status.cod!=Tasktrack.OK) {
                return {cod:task.ERR_DB, task:""}; //Si no se pudo agregar el Track, mandar el error al Task
            }
        }
        if(modifiedTask.urgency.description != originaltaskUrgency.description){
            let status = await Tasktrack.addTasktrackNoPush(ModificationDate,'Urgency','Urgency changed from "' + originaltaskUrgency.description + '" to "' + modifiedTask.urgency.description + '".', modifiedTask); //Agregar el nuevo Track
            if (status.cod!=Tasktrack.OK) {
                return {cod:task.ERR_DB, task:""}; //Si no se pudo agregar el Track, mandar el error al Task
            }
        }
    }
    else{ //Si no existe
        let status = await Tasktrack.addTasktrackNoPush(ModificationDate,'New','Task created.', modifiedTask); //Agregar el nuevo Track
        if (status.cod!=Tasktrack.OK) {
            return {cod:task.ERR_DB, task:""}; //Si no se pudo agregar el Track, mandar el error al Task
        }
    }
    return {cod:task.OK, task:modifiedTask};
}

module.exports=Tasktrack;