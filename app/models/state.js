const path=require('path');
const db=require(path.join(__dirname, '../../source', 'db.js'));

var stateSchema={
    description:{
        type: String,
        maxlength:20,
        minlength:5,
        required: "La descripcion es requerida",
        unique:true
    },
    tasks: [{type:db.Schema.ObjectId, ref: "Task"}]
}

var state_schema=new db.Schema(stateSchema); //Crear schema
var State=db.model("State", state_schema); //Crear o asumir modelo/coleccion en Mongo


State.OK=0;
State.ERR_DB=1;

//funciones
State.addState=async (state) => {
    let newSate = new State({description:state}) //Crear objeto State con el input
    try{
        await newSate.save();
        console.log("Estado creado en base");
        return State.OK;
    }
    catch(err){
        console.log(err);
        return State.ERR_DB;
    }
}

State.updateState=async (id, state) => {
    let newSate = new State({_id:id, description:state}) //Crear objeto State con el input
    try{
        await State.findByIdAndUpdate(id,newSate);
        console.log("Estado actualizado en base");
        return State.OK;
    }
    catch(err){
        console.log(err);
        return State.ERR_DB;
    }
}

State.deleteState=async (state_id) => {
    try{
        await State.deleteOne({_id:state_id});
        console.log("Estado borrado en base");
        return State.OK;
    }
    catch(err){
        console.log(err);
        return State.ERR_DB;
    }
}

State.FillStates=async () => {

    await State.deleteMany({}); //Borrar los valores actuales

    let States=[
        {description: 'In progress'},
        {description: 'On Hold'},
        {description: 'Request to User'},
        {description: 'On review'},
        {description: 'Closed'},
        {description: 'Discarded'}
    ];

    for (state of States) {
        let newSate = new State(state);
        await newSate.save();
    }
}

module.exports=State;