const path=require('path');
const db=require(path.join(__dirname, '../../source', 'db.js'));

var groupSchema={
    name:{
        type: String,
        maxlength:15,
        minlength:3,
        required: "El nombre es requerido",
        unique:true
    },
    description:{
        type: String,
        maxlength:200,
        minlength:5,
        required: "La descripcion es requerida",
        unique:true
    },
    users: [{type:db.Schema.ObjectId, ref: "User"}],
    tasks: [{type:db.Schema.ObjectId, ref: "Task"}]
}

var group_schema=new db.Schema(groupSchema); //Crear schema
var Group=db.model("Group", group_schema); //Crear o asumir modelo/coleccion en Mongo

Group.OK=0;
Group.ERR_DB=1;

//funciones
Group.addGroup=async (group, desc) => {
    let newGroup = new Group({name:group, description:desc}) //Crear objeto Group con el input
    try{
        await newGroup.save();
        console.log("Grupo creado en base");
        return Group.OK;
    }
    catch(err){
        console.log(err);
        return Group.ERR_DB;
    }
}

Group.updateGroup=async (id, group, desc) => {
    let newGroup = new Group({_id:id, name:group, description:desc}) //Crear objeto Group con el input
    try{
        await Group.findByIdAndUpdate(id,newGroup);
        console.log("Grupo actualizado en base");
        return Group.OK;
    }
    catch(err){
        console.log(err);
        return Group.ERR_DB;
    }
}

Group.deleteGroup=async (group_id) => {
    try{
        await Group.deleteOne({_id:group_id});
        console.log("Grupo borrado en base");
        return Group.OK;
    }
    catch(err){
        console.log(err);
        return Group.ERR_DB;
    }
}

module.exports=Group;