const path=require('path');
const db=require(path.join(__dirname, '../../source', 'db.js'));
var bcrypt=require("bcrypt");
var group=require("./group");

const SALT_ROUND=10;

var userSchema={
    name:{
        type: String,
        maxlength:50,
        minlength:5,
        required: "El nombre es requerido",
        unique:true
    },
    password:{
        type:String,
        minlength:20,
        required: "El nombre es requerido"
    },
    job:{
        type: String,
        maxlength:50,
        minlength:3,
        required: "La posicion es requerida",
        unique:true
    },
    group: {type: db.Schema.Types.ObjectId, ref:"Group"},
    assigned_tasks: [{type:db.Schema.ObjectId, ref: "Task"}],
    requested_tasks: [{type:db.Schema.ObjectId, ref: "Task"}]
}

var user_schema=new db.Schema(userSchema);
var User=db.model("User", user_schema);


//Errores
User.OK=0
User.ERR_DB=1;
User.ERR_PWD_NO_COINCIDEN=2;
User.ERR_PWD_CORTO=3;
User.ERR_USR_NO_VALIDO=4;
User.ERR_PWD_NO_VALIDO=5;

//funciones
User.addUser=async (user, grp, jb, pwd, pwd2)=>{
    if (pwd!=pwd2) return {cod:User.ERR_PWD_NO_COINCIDEN,id:""};
    if (pwd.length<8) return {cod:User.ERR_PWD_CORTO,id:""};
    let hash=await bcrypt.hash(pwd, SALT_ROUND);
    let userGroup=await group.findOne({name:grp}); //Buscar grupo
    let newUser=new User({name:user, password:hash, job:jb}); //Crear usuario
    newUser.group=userGroup; //Asignar grupo al usuario
    userGroup.users.push(newUser); //Asignar usuario a la coleccion de usuarios del grupo

    try{
        await newUser.save();
        await userGroup.save();
        console.log("Usuario creado en base");
        return {cod:User.OK, id:newUser._id};
    }catch(err){
        console.log(err);
        return {cod:User.ERR_DB,id:""};
    }
}

User.updateUser=async (user_id,user_name, user_job, grp, pwd,pwd2)=>{
    let userGroup=await group.findOne({name:grp}); //Buscar Grupo
    let newUser = await User.findOne({_id:user_id});
    newUser.name = user_name;
    newUser.group=userGroup; //Asignar grupo al usuario
    newUser.job = user_job;
    if (pwd && pwd2){
        if (pwd!=pwd2) return {cod:User.ERR_PWD_NO_COINCIDEN,id:""};
        if (pwd.length<8) return {cod:User.ERR_PWD_CORTO,id:""};
        let hash=await bcrypt.hash(pwd, SALT_ROUND);
        newUser.password = hash;
    }
    try{
        await newUser.save();
        console.log("Usuario actualizado en base");    
        return {cod:User.OK, id:newUser._id};
    }catch(err){
        console.log(err);
        return {cod:User.ERR_DB, id:""};
    }
};

User.deleteUser=async (user_id) => {
    try{
        await User.deleteOne({_id:user_id});
        console.log("Urgencia borrado en base");
        return User.OK;
    }
    catch(err){
        console.log(err);
        return User.ERR_DB;
    }
}

User.login=async (user,pwd)=>{
    try {
        let u=await User.findOne({name:user});
        if (!u) return {cod:User.ERR_USR_NO_VALIDO, id:""};
        let st=await bcrypt.compare(pwd,u.password);
        if (st) {
            return {cod:User.OK, id:u._id};
        }  
        else {
            return {cod:User.ERR_PWD_NO_VALIDO, id:""};
        }   
    } catch (err) {
        console.log(err);
        return {cod:User.ERR_DB, id:""};
    }
    
}



module.exports=User;