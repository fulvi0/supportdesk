const path=require('path');
const db=require(path.join(__dirname, '../../source', 'db.js'));
var state=require("./state.js");
var group=require("./group.js");
var user=require("./user.js");
var impact=require("./impact.js");
var urgency=require("./urgency.js");
var tasktrack=require("./tasktrack.js");
var Task=require("./taskinfo.js");

/*var taskSchema={
    name:{
        type: String,
        maxlength:50,
        minlength:5,
        required: "El nombre es requerido"
    },
    eta:{
        type: Number,
        required: "El ETA es requerido"
    },
    creation_date:{
        type: Date,
        requeried: "La fecha de creacion es requerida"
    },
    eta_date:{
        type: Date,
        requeried: "La fecha estimada de ETA es requerida"
    },
    closed_date:{
        type: Date
    },
    description:{
        type: String,
        maxlength:500,
        minlength:5,
        required: "La descripcion es requerida"
    },
    resolution:{
        type: String,
        maxlength:500,
        minlength:5
    },
    impact: {type: db.Schema.Types.ObjectId, ref:"Impact"},
    urgency: {type: db.Schema.Types.ObjectId, ref:"Urgency"},
    state: {type: db.Schema.Types.ObjectId, ref:"State"},
    group: {type: db.Schema.Types.ObjectId, ref:"Group"},
    requester: {type: db.Schema.Types.ObjectId, ref:"User"},
    responsible: {type: db.Schema.Types.ObjectId, ref:"User"},
    tracks: [{type:db.Schema.ObjectId, ref: "TaskTrack"}]
}

var task_schema=new db.Schema(taskSchema); //Crear schema
var Task=db.model("Task", task_schema); //Crear o asumir modelo/coleccion en Mongo

Task.OK=0;
Task.ERR_DB=1;
Task.NO_CLOSE_DATE=2;
Task.NO_CLOSE_STATUS=3;*/

//funciones
Task.addTask=async (task_name, eta, impct, urgcy, desc, resol, stt, grp, reqtr, respon, crt_dt, eta_dt, cls_date) => {
    let taskGroup = await group.findOne({name:grp}); //Buscar grupo
    let taskState = await state.findOne({description:stt}); //Buscar estado
    let taskRequester = await user.findOne({name:reqtr}); //Buscar requester
    let taskResponsible = await user.findOne({name:respon}); //Buscar responsable
    let taskImpact = await impact.findOne({description:impct}); // Buscar Impacto
    let taskUrgency = await urgency.findOne({description:urgcy}); // Buscar Urgencia
    let creationDate = Date.now();
    let newTask = new Task({name:task_name, eta:eta, description: desc, creation_date:crt_dt, eta_date:eta_dt}) //Crear objeto Task con el input

    if (resol){ //Poner resolucion solo si no esta vacia
        newTask.resolution = resol;
    }

    if (cls_date){ //Si la fecha de cierre no es nula
        if (resol != 'Closed')
            return Task.NO_CLOSE_STATUS; //Si hay fecha de cierre y el estado no es cerrado
        else
            newTask.closed_date = cls_date; //Agregar la fecha de cierre
    } else { //Si la fecha de cierre es nula
        if (resol == 'Closed')
            return Task.NO_CLOSE_DATE; //Si el estado es cerrado y no hoy fecha de cierre
    }

    newTask.group = taskGroup; //Asignar grupo al task
    newTask.state = taskState; //Asignar estado al task
    newTask.requester = taskRequester; //Asignar requester al task
    newTask.responsible = taskResponsible; //Asignar responsable al task
    newTask.impact = taskImpact; //Asignar impacto al task
    newTask.urgency = taskUrgency; //Asignar urgencia al task
    newTask.creation_date = creationDate;
    newTask.eta_date = creationDate + eta*60*60*1000;

    taskGroup.tasks.push(newTask); //Asignar task a la coleccion de tasks del grupo
    taskState.tasks.push(newTask); //Asignar task a la coleccion de tasks del estado
    taskRequester.requested_tasks.push(newTask); //Asignar task a la tasks de user del requester
    taskResponsible.assigned_tasks.push(newTask); //Asignar task a la tasks de user del responsable
    taskImpact.tasks.push(newTask); //Asignar task a la coleccion de tasks del impacto
    taskUrgency.tasks.push(newTask); //Asignar task a la coleccion de tasks de la urgencia

    try{
        let status = await tasktrack.addMultipleTasktracks(newTask, creationDate); //Agregar todos los cambios al tracking
        if (status.cod == Task.ERR_DB)
            return Task.ERR_DB; //Devoler error si no fue posible
        else
            await status.task.save(); //Guardar todos los cambios luego de agregar los tracks del task
        await taskGroup.save();
        await taskState.save();
        await taskRequester.save();
        await taskResponsible.save();
        await taskImpact.save();
        await taskUrgency.save();
        console.log("Task creado en base");
        return Task.OK;
    }
    catch(err){
        console.log(err);
        return Task.ERR_DB;
    }
}

Task.updateTask=async (task_id, task_body) => {
    let taskGroup = await group.findOne({name:task_body.task_group}); //Buscar grupo
    let taskState = await state.findOne({description:task_body.task_state}); //Buscar estado
    let taskRequester = await user.findOne({name:task_body.task_requester}); //Buscar requester
    let taskResponsible = await user.findOne({name:task_body.task_responsible}); //Buscar responsable
    let taskImpact = await impact.findOne({description:task_body.task_impact}); // Buscar Impacto
    let taskUrgency = await urgency.findOne({description:task_body.task_urgency}); // Buscar Urgencia
    //let newTask = new Task({code:task_code, name:task_name, eta:eta, priority:prty, urgency:urgcy, resolution: resol}) //Crear objeto Task con el input
    let changeDate = Date.now();
    let newTask = await Task.findOne({_id:task_id});

    newTask.name = task_body.task_name;
    newTask.eta = task_body.task_eta;
    newTask.description = task_body.task_description;

    if (task_body.task_resolution){ //Poner resolucion solo si no esta vacia
        newTask.resolution = task_body.task_resolution;
    }

    if (taskState.description == 'Closed')
        newTask.closed_date=changeDate;
    else
        newTask.closed_date=null;

    /*if (task_body.task_closed_date){ //Si la fecha de cierre no es nula
        if (taskState.description != 'Closed')
            return Task.NO_CLOSE_STATUS; //Si hay fecha de cierre y el estado no es cerrado
        else
            newTask.closed_date = task_body.task_closed_date; //Agregar la fecha de cierre
    } else if (taskState.description == 'Closed'){ //Si la fecha de cierre es nula
            newTask.closed_date=changeDate; //return Task.NO_CLOSE_DATE; //Si el estado es cerrado y no hoy fecha de cierre
    }*/

    newTask.group = taskGroup; //Asignar grupo al task
    newTask.state = taskState; //Asignar estado al task
    newTask.requester = taskRequester; //Asignar requester al task
    newTask.responsible = taskResponsible; //Asignar responsable al task
    newTask.impact = taskImpact; //Asignar impacto al task
    newTask.urgency = taskUrgency; //Asignar urgencia al task

    try{
        let status = await tasktrack.addMultipleTasktracks(newTask, changeDate); //Agregar todos los cambios al tracking
        if (status.cod == Task.ERR_DB)
            return Task.ERR_DB; //Devolver error si no fue posible
        else
            await status.task.save(); //Guardar todos los cambios luego de agregar los tracks del task
        console.log("Task actualizado en base");
        return Task.OK;
    }
    catch(err){
        console.log(err);
        return Task.ERR_DB;
    }
}

Task.deleteTask=async (task_id) => {
    try{
        await Task.deleteOne({_id:task_id});
        console.log("Estado borrado en base");
        return Task.OK;
    }
    catch(err){
        console.log(err);
        return Task.ERR_DB;
    }
}

module.exports=Task;