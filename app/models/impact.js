const path=require('path');
const db=require(path.join(__dirname, '../../source', 'db.js'));

var impactSchema={
    impact_value:{
        type: Number,
        required: "El valor es requerido"
    },
    description:{
        type: String,
        maxlength:30,
        minlength:5,
        required: "La descripcion es requerida",
        unique:true
    },
    tasks: [{type:db.Schema.ObjectId, ref: "Task"}]
}

var impact_schema=new db.Schema(impactSchema); //Crear schema
var Impact=db.model("Impact", impact_schema); //Crear o asumir modelo/coleccion en Mongo


Impact.OK=0;
Impact.ERR_DB=1;

//funciones
Impact.addImpact=async (value,desc) => {
    let newImpact = new Impact({impact_value:value, description:desc}) //Crear objeto Impact con el input
    try{
        await newImpact.save();
        console.log("Impacto creado en base");
        return Impact.OK;
    }
    catch(err){
        console.log(err);
        return Impact.ERR_DB;
    }
}

Impact.updateImpact=async (impact_id, value, desc) => {
    //let newImpact = new Impact({_id:impact_id, impact_value:value, description:desc}) //Crear objeto Impact con el input
    let newImpact = Impact.findOne({_id:impact_id});

    newImpact.impact_value = value;
    newImpact.description = desc;

    try{
        await newImpact.save();
        console.log("Estado actualizado en base");
        return Impact.OK;
    }
    catch(err){
        console.log(err);
        return Impact.ERR_DB;
    }
}

Impact.deleteImpact=async (impact_id) => {
    try{
        await Impact.deleteOne({_id:impact_id});
        console.log("Impacto borrado en base");
        return Impact.OK;
    }
    catch(err){
        console.log(err);
        return Impact.ERR_DB;
    }
}

Impact.FillImpacts=async () => {

    await Impact.deleteMany({}); //Borrar los valores actuales

    let Impacts=[
        {impact_value: '1', description: 'Produccion'},
        {impact_value: '2', description: 'Regulatorio'},
        {impact_value: '3', description: 'Entrenamiento o Reunion'},
        {impact_value: '4', description: 'Proyecto'},
        {impact_value: '5', description: 'Prueba'}
    ];

    for (impact of Impacts) {
        let newImpact = new Impact(impact);
        await newImpact.save();
    }
}

module.exports=Impact;