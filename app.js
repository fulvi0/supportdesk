var path = require('path');
var express = require('express');
var router_dashboard = require(path.join(__dirname, 'app', 'controllers', 'dashboard.js'));
var router_user_app = require(path.join(__dirname, 'app', 'controllers', 'userarea.js'));
var router_group_app = require(path.join(__dirname, 'app', 'controllers', 'grouparea.js'));
var router_task_app = require(path.join(__dirname, 'app', 'controllers', 'tasksarea.js'));
var router_load_app = require(path.join(__dirname, 'app', 'controllers', 'loadarea.js'));
var sessVal = require(path.join(__dirname, 'app', 'middlewares', 'sessionValidator.js'));
//var RedisStore = require('connect-redis')(session);
var db = require(path.join(__dirname, 'source', 'db.js'));

var server = express();

/*var session = session({
    store: new RedisStore({
        url: "//localhost:6379"
    }),
    secret: 'keyboard',
    resave: false,
    saveUninitialized: false
});*/

server.set('view engine', 'pug');

server.use(express.json());
server.use(express.urlencoded({
    extended: false
}));

server.get('/', function (req, res) {
    res.render('../app/views/taskArea/task_admin');
});

server.use('/supportdesk', router_dashboard);
server.use('/supportdesk', router_user_app);
server.use('/supportdesk', router_group_app);
server.use('/supportdesk', router_task_app);
server.use('/supportdesk', router_load_app);
//server.use("/supportdesk", sessVal);
server.use('/static', express.static(path.join(__dirname, 'public')));

server.listen(3000);
