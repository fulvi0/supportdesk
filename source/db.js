var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
var env = process.env.NODE_ENV || 'development';
var config = require('./mongo-env')[env];
console.log(config);

var envUrl = process.env[config.use_env_variable];
var localUrl = `mongodb://${config.host}/${config.database}`;
var mongoUrl = envUrl ? envUrl : localUrl;
console.log(mongoUrl);
mongoose.connect(mongoUrl);

module.exports=mongoose;